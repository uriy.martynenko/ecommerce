from django.urls import path

from base.views import user_views as views

# from rest_framework_simplejwt.views import (
#     TokenObtainPairView,
# )

urlpatterns = [
    path('', views.getUsers, name="users"),
    path('login/', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('register/', views.registerUser, name='register'),
    path('profile/', views.getUserProfile, name="users-profile"),
    path('profile/update/', views.updatetUserProfile, name="user-profile-update"),
    path('<str:pk>/', views.getUsersById, name='user'),
    path('update/<str:pk>/', views.updatetUser, name='user-update'),
    path('delete/<str:pk>/', views.deleteUser, name='user-delete'),
]

import {Container} from 'react-bootstrap';
import Header from './components/Header';
import Footer from './components/Footer';
import HomeScreen from './screens/HomeScreen';
import ProductScreen from "./screens/ProductScreen";
import {HashRouter as Router, Route, Routes} from "react-router-dom";
import CartScreen from "./screens/CartScreen";
import LoginScreen from "./screens/LoginScreen";
import RegisterScreen from "./screens/RegisterScreen";
import ProfileScreen from "./screens/ProfileScreen";
import ShippingScreen from "./screens/ShippingScreen";
import PaymentScreen from "./screens/PaymentScreen";
import PlaceOrderScreen from "./screens/PlaceOrderScreen";
import OrderScreen from "./screens/OrderScreen";
import UserListScreen from "./screens/UserListScreen";
import UserEditScreen from "./screens/UserEditScreen";
import ProductListScreen from "./screens/ProductListScreen";
import ProductEditScreen from "./screens/ProductEditScreen";
import OrderListScreen from "./screens/OrderListScreen";

function App() {
    return (
        <Router>
            <Header></Header>
            <main className='py-3'>
                <Container>
                    <Routes>
                        <Route path="/" Component={HomeScreen} exact></Route>
                        <Route path="/login" Component={LoginScreen}></Route>
                        <Route path="/register" Component={RegisterScreen}></Route>
                        <Route path="/profile" Component={ProfileScreen}></Route>
                        <Route path="/login/shipping" Component={ShippingScreen}></Route>
                        <Route path="/placeorder" Component={PlaceOrderScreen}></Route>
                        <Route path="/order/:id" Component={OrderScreen}></Route>
                        <Route path="/payment" Component={PaymentScreen}></Route>
                        <Route path="/product/:id" Component={ProductScreen}></Route>
                        <Route path="/cart/:id?" Component={CartScreen}></Route>
                        <Route path="/admin/userlist" Component={UserListScreen}></Route>
                        <Route path="/admin/user/:id/edit" Component={UserEditScreen}></Route>
                        <Route path={"/admin/productlist"} Component={ProductListScreen}></Route>
                        <Route path="/admin/product/:id/edit" Component={ProductEditScreen}></Route>
                        <Route path={"/admin/orderlist"} Component={OrderListScreen}></Route>
                    </Routes>
                </Container>
            </main>
            <Footer></Footer>
        </Router>
    );
}

export default App;

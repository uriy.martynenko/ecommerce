import React, {useState} from "react";
import {Form} from "react-bootstrap";
import {useLocation, useNavigate} from "react-router-dom";

function SearchBox() {
    const [keyword, setKeyword] = useState('')
    let history = useNavigate()
    let location = useLocation()
    const submitHandler = (e) => {
        e.preventDefault()

        if (keyword) {
            history(`/?keyword=${keyword}&page=1`)
        } else {
            history(history(location.pathname))
        }
    }

    return (
        <Form onSubmit={submitHandler}>
            <Form.Control
                type={'text'}
                name={'q'}
                onChange={(e) => setKeyword(e.target.value)}
                className={'p-1'}
                variant={'outline-success'}
            ></Form.Control>
        </Form>
    )
}

export default SearchBox
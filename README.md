<h1 align="center">ECOMMERCE</h1>

<p align="center">

  <img src="media/giphy.gif" width="100"/>
  <img src="media/cover.png.jpeg" width="150"/>
  <img src="media/Снимок экрана 2023-04-17 в 23.07.40.png" />
  <img src="media/Снимок экрана 2023-04-16 в 23.19.50.png" />
  <img src="media/Снимок экрана 2023-04-16 в 23.23.56.png" />
  <img src="media/Снимок экрана 2023-04-02 в 12.20.46.png" />
  <img src="media/Снимок экрана 2023-04-02 в 12.21.12.png" />
  <img src="media/Снимок экрана 2023-04-02 в 22.02.14.png" />
  <img src="media/Снимок экрана 2023-04-07 в 23.45.18.png" />
  <img src="media/Снимок экрана 2023-04-07 в 23.38.52.png" />
  <img src="media/Снимок экрана 2023-04-09 в 00.45.10.png" />
  <img src="media/Снимок экрана 2023-04-12 в 01.34.58.png" />
  <img src="media/Снимок экрана 2023-04-12 в 01.59.07.png" />
  <img src="media/Снимок экрана 2023-04-12 в 23.25.46.png" />
  <img src="media/Снимок экрана 2023-04-13 в 20.39.21.png" />
  <img src="media/Снимок экрана 2023-04-14 в 00.03.55.png" />
  <img src="media/Снимок экрана 2023-04-15 в 08.54.49.png" />
  <img src="media/Снимок экрана 2023-04-15 в 19.37.09.png" />
  <img src="media/Снимок экрана 2023-04-16 в 13.21.33.png" />
  <img src="media/Снимок экрана 2023-04-16 в 14.27.24.png" />
  <img src="media/Снимок экрана 2023-04-16 в 17.42.00.png" />
</p>

<hr style="border: double">
<h2>Структура базы данны SQL</h2>
<p align="center">
  <img src="backend/drawSQL-ecommerce-export-2023-03-25.png" />
</p>

<h2>Установка зависимостей</h2>

```
$ pip install -r requirements.txt
```

<hr style="border: double">

<hr style="border: double">
<h2>Запуск проекта</h2>
<p>
Для того что-бы запустить <b>ПРОЕКТ</b> перейдите в папку <b>backend</b> и выполните команду:
</p>

```angular2html
$ ./manage.py runserver
```



# FRONTEND

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more
information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will
remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right
into your project so you have full control over them. All of the commands except `eject` will still work, but they will
point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you
shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't
customize it when you are ready for it.

## Learn More

You can learn more in
the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved
here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved
here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved
here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved
here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved
here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved
here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)

<p>
Фронт написан на <b>React.</b><br>
Для того что-бы запустить <b>Фронт</b> перейдите в папку <b>frontend</b> и выполните команду:
</p>

```
$ npm start
```